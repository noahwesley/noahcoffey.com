/* NoahCoffey.com was made by Noah Coffey in Indianapolis, IN */

/* SITE */
  Standards: HTML5, CSS3
  Components: jQuery, Reveal
  Software: Coda, Git
  
/* PERSONAL */
  Food: Pizza, Sushi, Hamburgers, Tacos
  Bourbon: Blanton's, Bulleit, Woodford Reserve, Maker's Mark, Johnnie Walker Black Label (I know), Old Grand-Dad
  Meal: Breakfast
  
  Does anyone ever read these? Say hello sometime. I'm friendly.