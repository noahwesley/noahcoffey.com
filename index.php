<?php
  date_default_timezone_set('America/Indiana/Indianapolis');

  // FanBase
  $dStart = new DateTime('2014-10-27');
  $dEnd  = new DateTime();
  $dDiff = $dStart->diff($dEnd);
  $fanbase_days = $dDiff->days;
  $fanbase_years = number_format($fanbase_days/365,1);
  
  // Mixwest
  $dStart = new DateTime('2015-08-06');
  $dEnd  = new DateTime();
  $dDiff = $dStart->diff($dEnd);
  $mixwest_days = number_format($dDiff->days);

  // Alive
  $dStart = new DateTime('1980-07-29');
  $dEnd  = new DateTime();
  $dDiff = $dStart->diff($dEnd);
  $noah_years = number_format($dDiff->format('%y'));

  // Married
  $dStart = new DateTime('2006-10-14');
  $dEnd  = new DateTime();
  $dDiff = $dStart->diff($dEnd);
  $noah_married = number_format($dDiff->format('%y'));


	$rss = new DOMDocument();
	$rss->load('http://remotechance.com/rss/');
	
	$feed = array();
	foreach ($rss->getElementsByTagName('item') as $node) {
	$item = array ( 
		'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
		'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
		'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
		'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
		);
	array_push($feed, $item);
	}

	$limit = 5;

?><!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/foundation.css" />
	<link rel="stylesheet" href="css/style.css">

	<script src="//use.typekit.net/waj7bla.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>

    <script src="js/vendor/modernizr.js"></script>
	<title>Noah Coffey</title>
</head>
<body>

  <div class="first">
  	<div class="row">
  		<div class="large-12 columns text-center">
  			<h1>Noah Coffey</h1>
  			<p class="intro">I enjoy creating things on the web and helping people use technology.</p>
  		</div>
  	</div>
  </div>
  
  <div class="second">	
  	<div class="row">
      <ul class="large-block-grid-4 small-block-grid-2">

        <li>
          <div class="box fanbase">
            <a href="http://growafanbase.com"><img src="images/fanbase-logo.png" alt="FanBase logo" /></a>
			<h2>I'm improving User Experience at FanBase everyday.</h2>
          </div>
        </li>

        <li>
          <div class="box me">
		  	<h2>Here I am at <a href="http://charitypong.com">CharityPong</a>.</h2>
          </div>
        </li>

        <li>
          <div class="box mixwest">
            <a href="http://mixwest.com"><img src="http://mixwest.com/img/mixwest-logo-white.png" width="175" alt="Mixwest logo" /></a>
            <h2>I'll help organize Mixwest again in <strong><?=$mixwest_days?></strong> days.</h2>
          </div>
        </li>

        <li>
          <div class="box me speaking">
		  	<h2>Occasionally, <a href="http://blog.noahcoffey.com/speaking/">I speak</a>.</h2>
          </div>
        </li>

        
      </ul>
  	</div>	
  </div>

  <div class="footer">
  	<div class="row">
  		<div class="large-12 columns">
        <ul class="icons large-block-grid-7 text-center">
          <li><a data-tooltip data-reveal-id="modal__email" href="http://bin.formstack.com/forms/noahcoffeycom_contact_form" title="Email Noah"><i class="fa fa-envelope"></i></a></li>
          <li><a data-tooltip href="http://twitter.com/noahcoffey" title="Twitter"><i class="fa fa-twitter"></i></a></li>
          <li><a data-tooltip href="http://www.linkedin.com/in/noahcoffey/" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
          <li><a data-tooltip href="http://dribbble.com/noahwesley" title="dribbble"><i class="fa fa-dribbble"></i></a></li>
          <li><a data-tooltip href="http://github.com/noahwesley" title="GitHub"><i class="fa fa-github"></i></a></li>
          <li><a data-tooltip href="http://flickr.com/noahwesley" title="Some of my photos"><i class="fa fa-flickr"></i></a></li>
          <li><a data-tooltip href="http://pay.noahcoffey.com" title="Send me money"><i class="fa fa-usd"></i></a></li>
        </ul>
  		</div>
  	</div>
  </div>
  
  <div class="row">
	  <div class="medium-12 columns text-center">
		  <div class="blog-post">
			  <h3>Recent blog posts:</h3>
				<?php
					for($x=0;$x<$limit;$x++) {
						if (!$feed[$x]) continue;
						$title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
						$link = $feed[$x]['link'];
						$description = $feed[$x]['desc'];
						echo '<strong><a href="'.$link.'" title="'.$title.'">'.$title.'</a></strong>';
						
						if ($x == (count($feed)-2)) {
							print " and ";
						}elseif ($x < (count($feed)-1)){
							print ", ";
						}
					}
					
					print ".";
				?>			  
		  </div>
	  </div>
  </div>


  <div id="modal__email" class="reveal-modal small overlay" data-reveal>
    <h2>Contact Noah</h2>
	<script type="text/javascript" src="http://bin.formstack.com/forms/js.php?581760-lN2iKoiU9T-v3"></script><noscript><a href="http://bin.formstack.com/forms/noahcoffeycom_contact_form" title="Online Form">Online Form - NoahCoffey.com Contact Form</a></noscript>
    <a class="close-reveal-modal">&#215;</a>
  </div>


    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
</body>
</html>
